﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Notify;
using Notify.Interfaces;

namespace DemoSuscriberApp
{
    public partial class Form1 : Form, InotifyDelegate
    {
        
        Suscriber suscriber;
        Publisher publisher;

        public Form1()
        {
            InitializeComponent();

            Factory f = new Factory(this);

            suscriber = f.makeSuscriber("RPF002", "DEMOAPP");
            // a donde nos suscribimos
            suscriber.suscribeTo("test");
            // metodo que se ejecutará cuando se recepcione una notificación
            suscriber.onObjectReceived += handleObjNotification;
            suscriber.consuming<NotificationExample>();

            publisher = f.makePublisher("RPF002", "DEMOAPP", "test");

        }

        private delegate void processDelegate(NotificationExample obj);

        public void handleObjNotification(object obj) { 
            processDelegate s = processObjNotification;
            this.Invoke(s, obj);        
        }

        public void processObjNotification(NotificationExample obj)
        {
            //MessageBox.Show(obj.nombre);
            label1.Text += "\n"+obj.nombre;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NotificationExample n = new NotificationExample();
            n.nombre = "Test notification";

            publisher.SendObjNotification(n);
            
        }
    }
}
